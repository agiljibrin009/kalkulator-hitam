
const fs =  require('fs');


const save = (num1, num2) => {
    const result = {
        num : num1,
        num2 : num2,
    };
    // buat path data
    const dataPath = 'repository/data.json';
    // membuat file data.json sebagai penampung data
    if(!fs.existsSync(dataPath)){
        fs.writeFileSync(dataPath, '[]', 'utf-8');
    };
    // membaca path yang ada di repository
    const loadPath = () => {
        const file = fs.readFileSync(dataPath, 'utf-8');
        const fileParse = JSON.parse(file);
        return fileParse;
    };

    const sum = loadPath();
    sum.push(result);

    // baca kembali file dan jadikan string karena jadi object json di dalam array
    fs.writeFileSync(dataPath, JSON.stringify(sum));
    return sum
};

module.exports = {save};