const fs = require('fs');



const loadPath = () => {
    const dataPath = 'repository/data.json';
    const file = fs.readFileSync(dataPath, 'utf-8');
    const fileParse = JSON.parse(file);
    return fileParse;
};

const view = () => {
    const data = loadPath();
    data.forEach(e => {
        const data1 = e.num;
        const data2 = e.num2;
        console.log(`Your sum ${data1} : ${data2}`)
    });
};


view();

module.exports = {view}