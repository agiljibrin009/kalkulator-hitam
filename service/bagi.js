const { save } = require("./saveSum");

const shared = (num1, num2) => {

    const sum1 = parseInt(num1);
    const sum2 = parseInt(num2);

    save(sum1, sum2);
    
    return sum1 / sum2;
};

module.exports = {shared};